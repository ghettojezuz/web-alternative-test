![wa-image](web-alternative/src/assets/img/wa-logo.png)
# Тестовое задание для "Web Alternative"
Тестовое задание для "Web Alternative" (HTML, SСSS, WebPack, Sprites)

Основные требования:
* Break-points: ```320px```, ```480px```, ```768px```;
* Минимальная ширина результата: ```320px```; максимальная ширина результата: ```768px```;
* БЭМ;
* Использование спрайтов;
* Mobile first;

## Использование

Для просмотра build-версии на хостинге firebase *(не обращать внимание на версию для десктопа)*:
1. Перейти на https://web-alternative-test.web.app/
2. PROFIT

Для запуска проекта на dev-сервере необходимо: 
1.  Клонировать репозиторий (```git clone git@gitlab.com:ghettojezuz/web-alternative-test.git```)
2.  Установить ```devDependencies``` из ```package.json``` с помощью ```npm```
3.  Ввести команду ```npm run dev``` в консоли, находясь в директории ```/web-alternative```

## Структура проекта
*  ```/web-alternative```
    *  ```/build``` (конфиги webpack)
    *  ```/dist``` (build версия проекта)
    *  ```/src``` 
        *  ```/assets``` (различные ресурсы)
            *  ```/fonts``` (шрифты)
            *  ```/img``` (изображения)
            *  ```/scss``` (scss код)
                *  ```/components``` (scss для компонентов)
                *  ```/utils``` (переменные, миксины и т.д.)
                *  ```/main.scss``` (основной файл стилей)
        * ```/index.html``` (основной html)
        * ```/index.js``` (входная точка для webpack)

## Контакты
* 📧 Почта: [rebim@yandex.ru](mailto:rebim@yandex.ru)
* 💬 Telegram: [@ghettojezuz](http://t.me/ghettojezuz)